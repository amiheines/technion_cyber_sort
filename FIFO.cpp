#include "FIFO.h"
#include "car.h"

FIFO::FIFO(){
}

FIFO::FIFO(Car *pNewCar){
  list.addHead(pNewCar);
}

Car *FIFO::pop(){
  return list.removeHead();
}

void FIFO::push(Car *pNewCar){
  list.addHead(pNewCar);
}

void FIFO::printme(){
  list.printme();
}