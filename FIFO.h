#ifndef FIFO_H
#define FIFO_H

#include "car.h"
#include "list.h"

class FIFO{
private:
  List list;
public:
  FIFO();
  FIFO(Car *pNewCar);
  void push(Car *pNewCar);
  Car *pop();
  int isEmpty();
  void printme();
};

#endif