#include "LIFO.h"
#include "car.h"

LIFO::LIFO(){
}

LIFO::LIFO(Car *pNewCar){
  list.addTail(pNewCar);
}

Car *LIFO::pop(){
  return list.removeHead();
}

void LIFO::push(Car *pNewCar){
  list.addTail(pNewCar);
}

void LIFO::printme(){
  list.printme();
}