#ifndef LIFO_H
#define LIFO_H

#include "car.h"
#include "list.h"

class LIFO{
private:
  List list;
public:
  LIFO();
  LIFO(Car *pNewCar);
  void push(Car *pNewCar);
  Car *pop();
  int isEmpty();
  void printme();
};

#endif