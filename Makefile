car: car.cpp
	g++ -c -o bin/car.o car.cpp

node: node.cpp
	g++ -c -o bin/node.o node.cpp

list: list.cpp
	g++ -c -o bin/list.o list.cpp

fifo: FIFO.cpp
	g++ -c -o bin/FIFO.o FIFO.cpp

lifo: LIFO.cpp
	g++ -c -o bin/LIFO.o LIFO.cpp

all: car node list fifo lifo
	g++ -o main.exe bin/node.o bin/LIFO.o bin/FIFO.o bin/list.o bin/car.o main.cpp
