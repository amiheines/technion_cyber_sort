// CAR.h
#ifndef CAR_H
#define CAR_H

#include <cstddef>
#include <iostream>

class Car
{
private:
  int iSpeed;
  int iId;
public:
  Car(int iId, int iSpeed);
  void printme();
};

#endif