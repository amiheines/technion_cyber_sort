#include "list.h"
#include "node.h"
#include "car.h"

List::List(){
  headNode = NULL;
  tailNode = NULL;
}

Car *List::removeHead(){
  if(headNode!=NULL){
    Node *pTmp = headNode;
    headNode = headNode->getNext();
    Car *pCar = pTmp->getCar();
    delete pTmp;
    return pCar;
  }
}

Car *List::removeTail(){
  if(tailNode!=NULL){
    Node *pTmp = tailNode;
    Node *pCurrent = headNode;
    while(pCurrent->getNext()!=tailNode){
      pCurrent = pCurrent->getNext();
    }
    tailNode = pCurrent;
    tailNode->setNext(NULL);
    Car *pCar = pTmp->getCar();
    delete pTmp;
    return pCar;
  }
}

void List::addHead(Car *pNewCar){
  Node *pTmp = headNode;
  Node *pNewNode = new Node(pNewCar);
  headNode = pNewNode;
  pNewNode->setNext(pTmp);
  if(tailNode==NULL){
    tailNode = pNewNode;
  }
}

void List::addTail(Car *pNewCar){
  Node *pTmp = tailNode;
  Node *pNewNode = new Node(pNewCar);
  tailNode = pNewNode;
  if(pTmp!=NULL){
    pTmp->setNext(tailNode);
  }
  if(headNode==NULL){
    headNode = tailNode;
  }
}

void List::printme(){
  std::cout << "List printed::::" << std::endl;
  Node *pCurrent = headNode;
  while(pCurrent!=NULL){
    pCurrent->printme();
    pCurrent = pCurrent->getNext();
  }
}


