#ifndef LIST_H
#define LIST_H

#include "node.h"
#include "car.h"

class List{
private:
  Node *headNode, *tailNode;
public:
  List();
  Car *removeHead();
  void addHead(Car *pNewCar);
  Car *removeTail();
  void addTail(Car *pNewCar);
  void printme();
};

#endif