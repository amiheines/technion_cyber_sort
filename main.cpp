#include <iostream>
#include "car.h"
#include "FIFO.h"
#include "LIFO.h"

int main() {
  std::cout << "FIFO=========" << std::endl;
  FIFO *pFIFO = new FIFO(new Car(702123, 80));
  pFIFO->printme();
  pFIFO->push(new Car(425321, 90));
  pFIFO->printme();
  pFIFO->push(new Car(734111, 120));
  pFIFO->printme();
  pFIFO->push(new Car(401333, 100));
  pFIFO->printme();
  pFIFO->push(new Car(892222, 85));
  pFIFO->printme();
  pFIFO->push(new Car(439633, 135));
  pFIFO->printme();

  pFIFO->pop();
  std::cout << "pop!::::" << std::endl;
  pFIFO->printme();

  pFIFO->pop();
  std::cout << "pop!::::" << std::endl;
  pFIFO->printme();

  Car *pC = pFIFO->pop();
  std::cout << "pop!::::" << std::endl;
  pFIFO->printme();

  std::cout << "The poped car::::" << std::endl;
  pC->printme();


  std::cout  << std::endl << "FIFO 2 =========" << std::endl;
  FIFO *pFIFO2 = new FIFO();
  pFIFO2->push(new Car(70212, 80));
  pFIFO2->printme();
  pFIFO2->push(new Car(42532, 90));
  pFIFO2->printme();
  pFIFO2->push(new Car(73411, 120));
  pFIFO2->printme();
  pFIFO2->push(new Car(40133, 100));
  pFIFO2->printme();
  pFIFO2->push(new Car(89222, 85));
  pFIFO2->printme();
  pFIFO2->push(new Car(43963, 135));
  pFIFO2->printme();

  pC = pFIFO2->pop();
  std::cout << "pop!::::" << std::endl;
  pFIFO2->printme();

  pC = pFIFO2->pop();
  std::cout << "pop!::::" << std::endl;
  pFIFO2->printme();

  pC = pFIFO2->pop();
  std::cout << "pop!::::" << std::endl;
  pFIFO2->printme();

  std::cout << "The poped car::::" << std::endl;
  pC->printme();

  std::cout  << std::endl << "LIFO=========" << std::endl;
  LIFO *pLIFO = new LIFO();
  pLIFO->push(new Car(702123, 80));
  pLIFO->printme();
  pLIFO->push(new Car(425321, 90));
  pLIFO->printme();
  pLIFO->push(new Car(734111, 120));
  pLIFO->printme();
  pLIFO->push(new Car(401333, 100));
  pLIFO->printme();
  pLIFO->push(new Car(892222, 85));
  pLIFO->printme();
  pLIFO->push(new Car(439633, 135));
  pLIFO->printme();

  pC = pLIFO->pop();
  std::cout << "pop!::::" << std::endl;
  pLIFO->printme();

  pC = pLIFO->pop();
  std::cout << "pop!::::" << std::endl;
  pLIFO->printme();

  pC = pLIFO->pop();
  std::cout << "pop!::::" << std::endl;
  pLIFO->printme();

  std::cout << "The poped car::::" << std::endl;
  pC->printme();

}
