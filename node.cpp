#include "node.h"

Node::Node(){
  pCar = NULL;
  pNext = NULL;
}

Node::Node(Car *pCar){
  this->pCar = pCar;
  pNext = NULL;
}

Node* Node::getNext(){
  return this->pNext;
}
void Node::setNext(Node *pNode){
  this->pNext = pNode;
}

Car* Node::getCar(){
  return this->pCar;
}
void Node::setCar(Car *pCar){
  this->pCar = pCar;
}

void Node::printme(){
  this->pCar->printme();
}