#ifndef NODE_H
#define NODE_H

#include "car.h"

class Node{
private:
  Car *pCar;
  Node *pNext;
public:
  Node();
  Node(Car *pCar);
  void setCar(Car *pCar);
  Car *getCar();
  void setNext(Node *pNode);
  Node *getNext();
  void printme();
};

#endif